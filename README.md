# openmw-docs README

This will be our repository for handling all images used in RTD documentation
for OpenMW. In the future, all docs may be contained here,
but for now, just the images.

## Image uploading

Please upload all images to the directory they *would* be in,
if all of the documentation was being hosted here as well.
This will require a merge request (MR),
but shouldn't require a full fork of the project.

This means placing them inside the appropriate _static directories.
Please add any additional directories to the official documentation,
as well as indicating them in conf.py for RTD.

## Using images

Once one of the maintainers has approved your MR,
simply navigate from the main project page to the image.
Right click and copy the image URL.
Use this URL while writing documentation.
DO NOT copy the URL for that page as it will not contain the actual image.

## Image saving requirements

<We plan to have a nice list of what to do when capturing and saving your image>

For now, just make sure there isn't visible JPG compression.
If it needs transparency, save as a PNG.
DO NOT add a drop shadow or anything else.
The shadow will be added automatically via the docs.